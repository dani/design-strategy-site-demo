import DefaultTheme from 'vitepress/theme'
import { Theme } from 'vitepress';

// Import overrides for theme custom properties and custom CSS styles.
import './custom.css';

const customTheme: Theme = {
	...DefaultTheme,
};

export default customTheme;

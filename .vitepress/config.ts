import { defineConfigWithTheme, DefaultTheme } from 'vitepress';
import { existsSync } from 'fs';
// import path from 'path';

export default defineConfigWithTheme( {
	lang: 'en-US',
	title: 'Design at Wikimedia',
	description: 'Making sharing of all human knowledge easy and joyful. For everyone.',
	base: '/', 
	srcDir: 'src',

	// Disable dark mode.
	appearance: false,

	markdown: {
		theme: 'dracula'
	},

	themeConfig: {
		logo: {
			src: '/assets/logo-Wikimedia.svg', alt: 'Wikimedia Foundation'
		},

		nav: [
			{ text: 'Strategy', link: '/strategy/', activeMatch: '/strategy/' },
			{ text: 'Blog', link: 'https://design.wikimedia.org/blog/' },
			{ text: 'Style Guide', link: 'https://design.wikimedia.org/style-guide/' },
		],

		socialLinks: [
			{ icon: 'github', link: 'https://github.com/wikimedia/' },
			{ icon: 'twitter', link: 'https://twitter.com/@WikimediaDesign' },

			{
				icon: { svg: '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"><title>Wikimedia Foundation</title><path d="M6.08 5.555a6.048 6.048 0 0 0 3.055 10.593v-7.54L6.08 5.556zm7.828.004-3.05 3.05v7.536a6.048 6.048 0 0 0 3.05-10.587z"/><path d="M3.414 2.89C1.424 4.69.164 7.287.168 10.173c.007 5.406 4.42 9.806 9.828 9.806 5.407 0 9.82-4.4 9.828-9.806.004-2.886-1.255-5.482-3.246-7.285L14.865 4.6a7.355 7.355 0 0 1 2.524 5.568c-.007 4.09-3.3 7.375-7.394 7.375S2.61 14.26 2.604 10.17a7.355 7.355 0 0 1 2.523-5.568L3.414 2.89z"/><circle cx="10" cy="3.32" r="3.32"/></svg>' },
				link: 'https://wikimediafoundation.org/'
			},

			{ 
				icon: { svg: '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"><title>Mailing List</title><path d="M0 8v8a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V8l-10 4z"/><path d="M2 2a2 2 0 0 0-2 2v2l10 4 10-4V4a2 2 0 0 0-2-2z"/></svg>' },
				link: 'https://lists.wikimedia.org/mailman/listinfo/design'
			},
		],

		sidebar: {
			"/strategy/": [
				{
					text: 'About',
					link: '/strategy/',
					collapsed: true,
					items: [
						{ text: 'Team', link: '/strategy/team/' },
						{ text: 'Collaborators', link: '/strategy/collaborators' }
					]
				},
				{
					text: 'Initiatives',
					link: '/strategy/initiatives/',
					collapsed: true,
					items: [
						{ text: 'Powered by People', link: '/strategy/initiatives/powered-by-people' },
						{ text: 'Online Social Behavior', link: '/strategy/initiatives/online-social-behavior' },
						{ text: 'Machine Augmentation', link: '/strategy/initiatives/machine-augmentation' },
						{ text: 'Sentiment & Perception', link: '/strategy/initiatives/sentiment-and-perception' },
						{ text: 'Research & Development', link: '/strategy/initiatives/research-and-development' },
						{ text: 'Numeric Exploration', link: '/strategy/initiatives/numeric-exploration' }
					]
				},
				{
					text: 'News',
					link: '/strategy/news/',
				},
				{
					text: 'Timeline',
					link: '/strategy/timeline/',
				},
				{
					text: 'Participate',
					link: '/strategy/participate/',
				},
				{
					text: 'Collaborate',
					link: '/strategy/collaborate/',
				}
			],
		},

		footer: {
			// TODO: Extend footer
			// Privacy policy
			// https://wikimediafoundation.org/privacy-policy/
			// Accessibility statement
			// https://design.wikimedia.org/accessibility-statement.html
			
			// message: '',
			copyright: 'Text is available under the <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="noopener">Creative Commons Attribution-ShareAlike 4.0 International</a>, additional terms may apply. Code is available under the MIT license.', 
		},
	}
} );

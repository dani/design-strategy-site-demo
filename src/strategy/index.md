---
title: Wikimedia Design Strategy
---

![Photo by Margeigh Novotny](design-strategy-2020-10@1.5x.jpg)
_Photo by Margeigh Novotny_

# {{ $frontmatter.title }}

The Design Strategy group supports [Product Design](https://design.wikimedia.org/) at the Foundation by providing the qualitative and quantitative studies that allow Product teams to make informed and inspired decisions. From explanations of what people do and want, to evaluations of what they experience, to experiments with new and emerging technologies, the research team answers the most urgent and impactful questions that stand in the way of delivering the [“sum of all knowledge, to all the world’s people, for free, forever”](https://meta.wikimedia.org/wiki/Strategy/Wikimedia_movement/2018-20).

## Initiatives

### Powered by People

Understanding the motivations, challenges, and strategies of the contributors to Wikimedia Projects.

[Read more](powered-by-people.html)

### Online Social Behavior

Examining the prosocial and disruptive dimensions of online culture - on and off-wiki.

[Read more](online-social-behavior.html)

### Machine Augmentation

Exploring machine-in-the-loop technologies to enable knowledge equity at scale.

[Read more](machine-augmentation.html)

### Sentiment & Perception

Understanding how our readers and contributors perceive and engage with our tools and processes.

[Read more](sentiment-perception.html)

### Research & Development

Exploring longer horizon vision through design-lead experiments and collaborations with third party partners.

[Read more](research-development.html)

### Numeric Exploration

Understanding reader, editor and wiki-specific phenomena through quantitative analysis.

[Read more](numeric-exploration.html)

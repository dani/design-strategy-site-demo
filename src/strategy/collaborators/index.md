---
title: Collaborators 
---

# {{ $frontmatter.title }}

The Design Strategy group at the Wikimedia Foundation often works with outside researchers and firms to augment it's capacity - in order to provide the insights our Product teams need to make informed decisions. The Design Strategy team is grateful for the opportunity to learn from other practitioners.

[AfriqInsights](https://www.afriqinsights.com/) is a market research agency that operates across Africa. AfriqInsights supported the Foundation on the 2021 African context Emerging Education report.

[Anagram Research](http://anagramresearch.com/) is a design research agency in Bangalore, India that supported the Foundation on research about multilingual editors and the Section Translation tool.

[Ari Natarina](https://www.linkedin.com/in/ari-natarina-ph-d-0a631024/) is a language researcher, linguist, and Indonesian language expert who worked with the WMF on the WikiStories Indonesia Concept Testing research.

[Berkman Klein Center for Internet and Society](https://cyber.harvard.edu/)'s Youth and Media team collaborated with the Wikimedia Foundation to research the habits and preferences of youth across Latin America on the HabLatAm project.

[Taryn Bipat](https://tarynbipat.me/) is a design researcher who composed a literature review for the WMF on Readability and Optimal Text Settings on Wikipedia.

[The Carnegie Mellon Human Computer Interaction Institute](https://www.hcii.cmu.edu/research) engaged with the Foundation in 2020 to explore the potential of extracting signals, inferences and indicators of trustworthiness from Wikipedia article talk pages.

[The Center for Information Technology at Princeton](https://citp.princeton.edu/) engaged with the Foundation in 2020 to better understand the sometimes disproportionate influence that the "loudest voices" in online communities can have on policies, decisions and normative behaviors that affect everyone in the community.

[Chat Mow, LLC](https://www.linkedin.com/in/mathewchow/) is an organizational design and innovation agency that provided the Foundation with a summary of research on diversity, equity, and inclusion in Wikimedia Foundation.

[Concept Hatchery](http://www.concepthatchery.com/) is a design research agency that lead the [Movement Organizers Study](https://meta.wikimedia.org/wiki/Movement_Organizers) in 2019. Concept Hatchery's founder and lead researcher, Ana Chang, lead the Design Strategy team between 2020 and 2022.

[Context Partners](https://www.contextpartners.com/) is an organizational design, strategy, and facilitation consultancy that lead the Parity Project, research into how the Foundation can make research and product design more equitable.

[Criba Research](http://www.cribaweb.com.ar/) is a consumer research firm that supported the WMF in the Desktop Improvements: Table of Contents and Sticky Header project and various usability testing projects in Spanish.

[FloLab](http://www.flolab.co) is a Jakarta based UI/UX design and research firm that partnered with us on the Wikistories Early Adopters research.

[David Goldberg](https://www.linkedin.com/in/david-a-m-goldberg-900961b2/) is an independent writer and theorist who supported the Foundation on the following projects: [Product Perspectives](https://www.mediawiki.org/wiki/Wikimedia_Product/Perspectives), [Wikimedia Foundation Design Blog](https://design.wikimedia.org/blog/).

[Jeff Howard](http://howardesign.com/4.1/) is a designer and design research who has contracted with the Foundation on the following research projects: Wikifunctions, Wikimail, developer portal personas, and more.

[HumanCraft](https://www.humancraft.com/) is a design research agency lead by [David Lubensky](https://www.linkedin.com/in/davidlubensky/) and [Rachel Lichte](https://www.linkedin.com/in/rachellichte/), who supported the Foundation on a quantitative sentiment analysis of machine-translated content.

[Hureo](http://hureo.com/) is a user research company in Pune, India, that supported the Foundation on the following research: [Mobile Wikipedia Users in India](India_Mobile_Personas.html), Machine Translation Meets Human Perception: Hindi, [Desktop Refresh](Desktop_Refresh.html), and KaiOS App usability.

[Logic Department](https://logicdept.com/) is a design research agency in Brooklyn, NY that supported the Foundation on an ethnographic study of [Mobile Readers and Editors of Wikipedia in the U.S.](US_Mobile_Personas.html)

[Jim Maddock](http://jmaddock.net/) is a Human Computer Interaction researcher whose work intersects with computer science, communications, and computational social science. Jim worked with the Design Strategy team as a data analyst on Long Tail Topic Ontology, Momentum, and Commons Research.

[NaTakallam](https://natakallam.com/) is a social enterprise that offers language, translation, and cultural exchange services delivered by displaced persons. NaTakallam supported the Foundation on the Machine Translation Meets Human Perception: Arabic study by supplying 185 native Arabic speaking research participants.

[Project Kobo](https://www.projectkobo.com/) is a Japan based market and user research agency that supported the Foundation on the Japanese portion of the Media Matching project.

[Anna Rader](https://acrader.com/) is an independent researcher, specializing in qualitative social science research, who has supported the Foundation on the studies: [Why Do People Edit Wikipedia?](why-do-people-edit-wikipedia.html), [How Do News Organizations Use Wikipedia?](https://upload.wikimedia.org/wikipedia/commons/2/2a/How_News_Media_Orgs_Use_Wikipedia_Working_Paper_30_June_2020.pdf), and [Interpersonal Communication on Wikipedia](https://upload.wikimedia.org/wikipedia/commons/7/74/Interpersonal_communication_and_coordination_in_Wikipedia_Working_Paper_30_June_2020.pdf).

[Michael Raish](https://www.linkedin.com/in/michael-raish-77619ab2/) is an independent researcher, applied linguist, and Arabic scholar, who supported the Foundation on the following projects: Machine Translation Meets Human Perception: Arabic, Hindi, Bahasa Indonesia, and a combined analysis of the three languages; [2019 Community Health Survey](https://meta.wikimedia.org/wiki/Community_health_initiative/Administrator_Confidence_Survey); and [Harassment on Arabic Wikipedia](https://meta.wikimedia.org/wiki/Research:Arabic_Harassment_Netnography). Mike joined the Foundation in February of 2021 as a lead design researcher.

[Reboot](https://reboot.org/) is a strategic design research agency in New York, NY that partnered with the Foundation to deliver the [New Readers](https://meta.wikimedia.org/wiki/New_Readers) and [New Editor Experiences](https://www.mediawiki.org/wiki/New_Editor_Experiences) studies.

[Mary Grace Reich](https://www.linkedin.com/in/marygracereich/) completed the [Positive Reinforcement](https://commons.wikimedia.org/wiki/File:\(GROWTH\)_Positive_Reinforcement_Synthesis.pdf) project as an intern for the team.

[Spiegel Institut](https://www.spiegel-institut.de/) is a consumer and user research consultancy based in Germany that worked with the WMF on the Verifiability on Wikipedia research.

[Teak Research](https://www.teakresearch.com/) is a design research company based in Thailand that collaborated with the WMF on Section translation Post-Improvements testing with Thai Wikipedia editors.

[Urika Research](http://www.urikaresearch.com/) is a research firm based in Ghana that partnered with the WMF for the Desktop Improvements: Table of Contents and Sticky Header research.

[UserTestingArabic](https://www.usertestingarabic.com/) is our go-to partner for user research and testing conducted in Arabic. UTA has partnered with us on Special Search and more.

[YUX](https://yux.design/) is a pan-African design and research firm that partnered with the WMF on Wikimedia on several projects including Campaigns, Social Movements research, and more.

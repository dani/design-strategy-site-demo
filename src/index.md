---
title: Design at Wikimedia Foundation
aside: false 
sidebar: false
isHomepage: true
---

# {{ $frontmatter.title }}

Design at Wikimedia makes sharing of all human knowledge easy and joyful. For everyone.

The Foundation Design team's goal is to ensure that Wikimedia products and communications follow a design process centered on the user. Based on research to understand people’s needs and motivations, we explore solutions that meet those needs.

We design in the open with a transparent and participatory process. We collaborate within the Wikimedia Foundation and with the global community of contributors. We create well-designed solutions, together.

## Design team

Wikimedia Foundation product design team is a multidisciplinary team with extensive experience in user experience design and research, engineering, information architecture, human-computer interaction, visual design, and usability. We’re always looking for designers to work with us.

* [View current job openings](https://wikimediafoundation.org/about/jobs/)
* [Learn more about the team](https://www.mediawiki.org/wiki/Design)
* [Learn about Design Strategy research initiatives](/about/)

## Design Style Guide

Resources and design principles for Wikimedia projects

[View Design Style Guide](https://design.wikimedia.org/style-guide/)

## Participate

Collaborate with us on design for Wikimedia projects

[Learn more](https://www.mediawiki.org/wiki/Design/Participate)

## Blog

Articles about how we approach design at Wikimedia

[Read more](https://design.wikimedia.org/blog/)

## Research

Qualitative and quantitative studies by the Design Strategy Group

[Explore](/about/)

import { defineConfig } from 'vite';

export default defineConfig( {
	server: {
		// Listen on all IP addresses, in case Vite is run inside a VM
		host: '0.0.0.0'
	},

	build: {
		minify: true
	},
} );
